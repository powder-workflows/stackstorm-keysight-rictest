from __future__ import print_function

import os
import sys
import time
import json
import requests
import logging
import urllib3
import copy
import zipfile
import io
import base64

from st2client.client import Client
from st2client.models import KeyValuePair

from config_template import default_config_template

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class KeysightRICtestAPI(object):

    def __init__(self, endpoint="https://localhost:443",
                 username="admin", password="admin", token=None,
                 token_expires_at=None, verify=False, logger=None,
                 **kwargs):
        self.logger = logger or logging.getLogger()
        self.st2client = Client("http://localhost")
        self.endpoint = endpoint
        self.username = username
        self.password = password
        self.session = requests.session()
        self.session.verify = verify
        self._token_kvp_name = "keysight_ric_token__" + self.username
        self._token = token or None
        self._token_expires_at = token_expires_at or None
        if not self._token:
            token_kvp = self.st2client.keys.get_by_name(
                name=self._token_kvp_name, decrypt=True) #, scope="user")
            if token_kvp:
                self.logger.debug("existing token: %r", self._token_kvp_name)
                self._token = token_kvp.value
                self._token_expires_at = None
                self.session.headers.update({ "Authorization": self._token })
            else:
                self.logger.debug("no existing token for key %r", self._token_kvp_name)

    def _url(self, path):
        sep = ""
        if not path.startswith("/"):
            sep = "/"
        return self.endpoint + sep + path

    def login(self, force=False):
        if self._token and self._token_expires_at \
          and time.time() < (self._token_expires_at - 10) \
          and not force:
            return

        self._token = None
        self._token_expires = None
        self.session.headers.update({ "Authorization": None })

        u = self._url("/auth/realms/keysight/protocol/openid-connect/token")
        data = dict(grant_type="password", username=self.username,
                    password=self.password, client_id="clt-wap")
        headers = { "Content-type": "application/x-www-form-urlencoded" }
        r = self.session.post(u, data=data, headers=headers)
        if r.status_code == requests.codes.ok:
            self._token = r.json()["access_token"]
            ttl = r.json()["expires_in"]
            self._token_expires_at = ttl + time.time()
            self.session.headers.update({ "Authorization": self._token })
            # XXX: we ought to restrict to scope=user so that only the invoking
            # user and admins can retrieve the API token, but client.keys.get_by_name
            # does not handle scope=user, so lookups fail.
            token_kvp = KeyValuePair(
                name=self._token_kvp_name, value=self._token, secret=True,
                ttl=ttl) #scope="user"
            self.st2client.keys.update(token_kvp)
            self.logger.debug("login succeeded (%d)", r.status_code)
            return
        else:
            self.logger.error("login failed (%d): %r", r.status_code, r.content)
            r.raise_for_status()

    def get(self, *args, **kwargs):
        self.logger.debug("get %r %r", args, kwargs)
        r = self.session.get(*args, **kwargs)
        if r.status_code == 401:
            self.login(force=True)
            self.logger.debug("get %r %r", args, kwargs)
            r = self.session.get(*args, **kwargs)
        return r

    def post(self, *args, **kwargs):
        self.logger.debug("post %r %r", args, kwargs)
        r = self.session.post(*args, **kwargs)
        if r.status_code == 401:
            self.login(force=True)
            self.logger.debug("post %r %r", args, kwargs)
            r = self.session.post(*args, **kwargs)
        return r

    def delete(self, *args, **kwargs):
        self.logger.debug("delete %r %r", args, kwargs)
        r = self.session.delete(*args, **kwargs)
        if r.status_code == 401:
            self.login(force=True)
            self.logger.debug("delete %r %r", args, kwargs)
            r = self.session.delete(*args, **kwargs)
        return r

    def agent_list(self):
        r = self.get(self._url("/api/v2/agents"))
        self.logger.debug("agent_list: %r", r.content)
        if r.status_code == requests.codes.ok:
            return (True, dict(code=r.status_code, data=r.json()))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def agent_find(self, agent_id):
        r = self.get(self._url("/api/v2/agents/" + str(agent_id) + "?include=all"))
        self.logger.debug("agent_find: %r", r.content)
        if r.status_code == requests.codes.ok:
            return (True, dict(code=r.status_code, data=r.json()))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def config_find(self, config_id):
        r = self.get(self._url("/api/v2/configs/" + str(config_id) + "?include=all"))
        self.logger.debug("config_find: %r", r.content)
        if r.status_code == requests.codes.ok:
            return (True, dict(code=r.status_code, data=r.json()))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def agent_find_first(self, agent_id, agent_interface):
        (rc, rd) = self.agent_list()
        if not rc:
            return (False, dict(code=rd["code"], data="Error listing agents: %r" % (rd["data"],)))
        selected_agent = None
        selected_agent_iface = None
        for agent in rd["data"]:
            if agent_id:
                if agent_id == agent["id"] or agent_id == agent["hostname"]:
                    self.logger.debug(
                        "matched requested agent %r (offline=%r)",
                        agent.get("id",None), agent.get("offline"))
                    selected_agent = agent
                    for iface in agent.get("Interfaces", []):
                        if agent_interface == iface.get("Name"):
                            selected_agent_iface = iface
                    if not selected_agent_iface:
                        return (False, dict(code=404, data="Requested agent does not have interface %r" % (agent_interface,)))
                    break
            if "offline" in agent and agent["offline"]:
                self.logger.debug("skipping offline agent %r", agent.get("id",None))
                continue
            for iface in agent.get("Interfaces", []):
                if agent_interface == iface.get("Name"):
                    selected_agent = agent
                    selected_agent_iface = iface
                    self.logger.debug(
                        "selected first online agent %r (iface=%r)",
                        agent.get("id",None), agent_interface)
                    break
            if selected_agent:
                break

        if not selected_agent:
            return (False, dict(code=404, data="Unable to find available agent (%r,%r)" % (agent_id, agent_interface,)))
        else:
            return (True, dict(code=200, data=dict(agent=selected_agent)))

    def config_create(self, config_template, config_name,
                      ric_e2_addr, ric_e2_port, ric_gw_addr, ric_gw_prefix,
                      oenb_count, ognb_count, ue_count,
                      sustain, delay, ramp_up_rate, ramp_down_rate,
                      agent_id, agent_interface):
        # If user didn't specify an agent, find one; if they did, collect details.
        # XXX If user didn't specify an agent interface, assume enp6s0.
        if not agent_interface:
            agent_interface = "enp6s0"
        mapped_agent = dict()
        (rc, rd) = self.agent_list()
        if not rc:
            return (False, dict(code=rd["code"], data="Error listing agents: %r" % (rd["data"],)))
        selected_agent = None
        selected_agent_iface = None
        for agent in rd["data"]:
            if agent_id:
                if agent_id == agent["id"] or agent_id == agent["hostname"]:
                    self.logger.debug(
                        "matched requested agent %r (offline=%r)",
                        agent.get("id",None), agent.get("offline"))
                    selected_agent = agent
                    for iface in agent.get("Interfaces", []):
                        if agent_interface == iface.get("Name"):
                            selected_agent_iface = iface
                    if not selected_agent_iface:
                        return (False, dict(code=404, data="Requested agent does not have interface %r" % (agent_interface,)))
                    break
            if "offline" in agent and agent["offline"]:
                self.logger.debug("skipping offline agent %r", agent.get("id",None))
                continue
            for iface in agent.get("Interfaces", []):
                if agent_interface == iface.get("Name"):
                    selected_agent = agent
                    selected_agent_iface = iface
                    self.logger.debug(
                        "selected first online agent %r (iface=%r)",
                        agent.get("id",None), agent_interface)
                    break
            if selected_agent:
                break
        if not selected_agent:
            return (False, dict(code=404, data="Unable to find available agent (%r,%r)" % (agent_id, agent_interface,)))
        else:
            mapped_agent = {
                "agentId": selected_agent.get("id"),
                "id": "",
                "interfaceMappings": [
                    {
                        "agentInterface": agent_interface,
                        "agentInterfaceMac": selected_agent_iface.get("Mac"),
                        "nodeInterface": "e2"
                    }
                ]
            }
        self.logger.debug("using mappedAgent=%r", mapped_agent)

        c = None
        if not config_template:
            c = copy.deepcopy(default_config_template)
        else:
            c = copy.deepcopy(config_template)
        c["displayName"] = config_name
        c["configData"]["Name"] = config_name
        c["configData"]["RICConfig"]["name"] = config_name
        
        for x in ("oducu", "odusplitcu"):
            if not x in c["configData"]["RICConfig"]["nodes"]:
                continue
            for y in ("ocuNodes", "oduNodes", "ocucpNodes", "ocuupNodes"):
                if not y in c["configData"]["RICConfig"]["nodes"][x]["simulated"]:
                    continue
                for z in c["configData"]["RICConfig"]["nodes"][x]["simulated"][y]:
                    z["interfaces"]["e2"]["connectivitySettings"]["gwStart"] = ric_gw_addr
                    z["interfaces"]["e2"]["connectivitySettings"]["ipPrefix"] = ric_gw_prefix
        for x in ("oenb", "ognb"):
            for z in c["configData"]["RICConfig"]["nodes"][x]["ranges"]:
                z["interfaces"]["e2"]["connectivitySettings"]["gwStart"] = ric_gw_addr
                z["interfaces"]["e2"]["connectivitySettings"]["ipPrefix"] = ric_gw_prefix
        ric = c["configData"]["RICConfig"]["nodes"]["ric"]["ranges"][0]
        ric["interfaces"]["e2"]["connectivitySettings"]["localIPAddress"] = ric_e2_addr
        ric["interfaces"]["e2"]["connectivitySettings"]["port"] = ric_e2_port

        for r in c["configData"]["RICConfig"]["nodes"]["oenb"]["ranges"]:
            if not oenb_count:
                r["enable"] = False
            else:
                r["enable"] = True
                r["count"] = oenb_count
        if not oenb_count:
            c["configData"]["RICConfig"]["nodes"]["oenb"]["settings"]["mappedAgents"] = []
        else:
            c["configData"]["RICConfig"]["nodes"]["oenb"]["settings"]["mappedAgents"] = [ mapped_agent ]
        for r in c["configData"]["RICConfig"]["nodes"]["ognb"]["ranges"]:
            if not ognb_count:
                r["enable"] = False
            else:
                r["enable"] = True
                r["count"] = ognb_count
        if not ognb_count:
            c["configData"]["RICConfig"]["nodes"]["ognb"]["settings"]["mappedAgents"] = []
        else:
            c["configData"]["RICConfig"]["nodes"]["ognb"]["settings"]["mappedAgents"] = [ mapped_agent ]
        for r in c["configData"]["RICConfig"]["nodes"]["ue"]["ranges"]:
            if not ue_count:
                r["enable"] = False
            else:
                r["enable"] = True
                r["count"] = ue_count
            for t in r["timelines"]:
                if sustain:
                    t["sustain"] = sustain
                if delay:
                    t["delay"] = delay
                if ramp_down_rate:
                    t["rampDownRate"] = ramp_down_rate
                if ramp_up_rate:
                    t["rampUpRate"] = ramp_up_rate

        r = self.post(self._url("/api/v2/configs"), json=c)
        self.logger.debug("config_create: %r %r", r, r.content)
        if r.status_code in (requests.codes.ok, requests.codes.created):
            j = r.json()[0]
            return (True, dict(code=r.status_code, data=j, config_id=j["id"]))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def config_delete(self, config_id):
        r = self.delete(self._url("/api/v2/configs/" + str(config_id)))
        self.logger.debug("config_delete: %r", r.content)
        if r.status_code in (requests.codes.ok, requests.codes.accepted, requests.codes.no_content):
            return (True, dict(code=r.status_code))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def session_find(self, session_id):
        r = self.get(self._url("/api/v2/session/" + str(session_id) + "?include=all"))
        self.logger.debug("session_find: %r", r.content)
        if r.status_code == requests.codes.ok:
            return (True, dict(code=r.status_code, data=r.json()))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def session_create(self, config_id):
        r = self.post(self._url("/api/v2/sessions"), json={ "ConfigUrl": config_id})
        self.logger.debug("session_create: %r", r.content)
        if r.status_code in (requests.codes.ok, requests.codes.created):
            j = r.json()[0]
            return (True, dict(code=r.status_code, data=j, session_id=j["id"]))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def session_delete(self, session_id):
        r = self.delete(self._url("/api/v2/sessions/%s" % (session_id,)))
        self.logger.debug("session_delete: %r", r.content)
        if r.status_code in (requests.codes.ok, requests.codes.accepted, requests.codes.no_content):
            return (True, dict(code=r.status_code))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def session_test_start(self, session_id, status_wait=60):
        r = self.post(self._url("/api/v2/sessions/%s/test-run/operations/start" % (session_id,)))
        self.logger.debug("session_test_start: %r", r.content)
        if r.status_code not in (requests.codes.ok, requests.codes.accepted):
            self.logger.error("failed to create a test for session %r", session_id)
            return (False, dict(code=r.status_code, error=r.content))
        tj = r.json()
        op_id = tj["id"]
        if not status_wait:
            return (True, dict(code=r.status_code, data=tj, test_id=op_id))

        while status_wait > 0:
            time.sleep(4)
            status_wait -= 4
            r = self.get(self._url("/api/v2/sessions/%s/test-run/operations/start/%s" \
                                   % (session_id, op_id)))
            if r.status_code != requests.codes.ok:
                self.logger.debug("status_wait test start status: %r", r.status_code)
                continue
            sj = r.json()
            if sj["state"] == "SUCCESS":
                return (True, dict(code=r.status_code, data=sj, test_id=sj["result"]["testId"]))
            if sj["state"] == "ERROR":
                self.logger.error("failed to collect generated csv results")
                code = sj["code"] if "code" in sj else r.status_code
                error = sj["message"] if "message" in sj else r.content.decode("utf8")
                return (False, dict(code=code, data=sj, error=error))
        return (False, dict(code=500, error="Test start failed to report status"))

    def session_test_status(self, session_id, test_id):
        r = self.get(self._url("/api/v2/sessions/%s/test-run/operations/start/%s" \
                               % (str(session_id), str(test_id))))
        self.logger.debug("session_test_status: %r", r.content)
        if r.status_code in (requests.codes.ok, requests.codes.created):
            j = r.json()
            return (True, dict(code=r.status_code, data=j))
        else:
            return (False, dict(code=r.status_code, error=r.content))

    def session_test_stop(self, session_id, status_wait=60):
        r = self.post(self._url("/api/v2/sessions/%s/test-run/operations/stop" % (session_id,)))
        self.logger.debug("session_test_stop: %r", r.content)
        if r.status_code not in (requests.codes.ok, requests.codes.accepted):
            self.logger.error("failed to stop a test for session %r", session_id)
            return (False, dict(code=r.status_code, error=r.content))
        tj = r.json()
        test_id = tj["id"]
        if tj.get("state",None) == "ERROR" and tj["progress"] == 100:
            self.logger.debug("session_test_stop: already stopped and completed")
            return (True, dict(code=r.status_code, data=tj))
        if not status_wait:
            return (True, dict(code=r.status_code, data=tj, test_id=tj["id"]))

        while status_wait > 0:
            time.sleep(4)
            status_wait -= 4
            r = self.get(self._url("/api/v2/sessions/%s/test-run/operations/stop/%s" \
                                   % (session_id, test_id)))
            if r.status_code != requests.codes.ok:
                self.logger.debug("status_wait test stop status: %r", r.status_code)
                continue
            sj = r.json()
            if sj["state"] == "SUCCESS" or sj.get("progress", 0) == 100:
                return (True, dict(code=r.status_code, data=sj))
            if sj["state"] == "ERROR":
                code = sj["code"] if "code" in sj else r.status_code
                error = sj["message"] if "message" in sj else r.content.decode("utf8")
                return (False, dict(code=code, data=sj, error=error))
        return (False, dict(code=500, error="Test stop failed to report status"))

    def results_generate_csv(self, test_id, status_wait=60):
        r = self.post(self._url("/api/v2/results/%s/operations/generate-csv" % (test_id,)))
        self.logger.debug("results_generate_csv: %r", r.content)
        if r.status_code not in (requests.codes.ok, requests.codes.accepted):
            self.logger.error("failed to gather results for test %r", test_id)
            return (False, dict(code=r.status_code, error=r.content))
        j = r.json()
        op_id = j["id"]

        e2errors = 0
        e2associations = 0
        e2shutdown = 0
        e2abort = 0
        e2oob = 0
        e2indications = 0
        e2stats = dict()

        while status_wait > 0:
            time.sleep(4)
            status_wait -= 4
            r = self.get(self._url("/api/v2/results/%s/operations/generate-csv/%s" \
                                   % (test_id, str(op_id))))
            if r.status_code != requests.codes.ok:
                self.logger.debug("results_generate_csv op status: %r", r.status_code)
                continue
            sj = r.json()
            if sj["state"] == "SUCCESS":
                files = dict()
                r = self.get(self._url(sj["resultUrl"]))
                ff = io.BytesIO(r.content)
                z = zipfile.ZipFile(ff)
                (config_data, config_metadata) = (None, None)
                for zn in z.namelist():
                    if not zn.endswith(".csv"):
                        continue
                    with z.open(zn) as zf:
                        files[zn] = zf.read().decode("utf8")
                    if zn == "Ricoverview_E2SCTPStats.csv":
                        lsa = files[zn].split("\n")[-2].split(",")
                        (e2associations, e2abort, e2shutdown, e2errors, e2oob) = \
                          (int(lsa[2]), int(lsa[3]), int(lsa[4]), int(lsa[5]), int(lsa[7]))
                    if zn == "Ricoverview_TotalE2RICIndications.csv":
                        try:
                            e2indications = int(files[zn].split("\n")[-2].split(",")[-1])
                        except:
                            e2indications = 0
                e2stats = dict(
                    e2errors=e2errors, e2associations=e2associations,
                    e2shutdown=e2shutdown, e2abort=e2abort, e2oob=e2oob,
                    e2indications=e2indications)
                return (True, dict(code=r.status_code, files=files, e2stats=e2stats)) # data=base64.b64encode(r.content),
            if sj["state"] == "ERROR":
                self.logger.error("failed to collect generated csv results")
                code = sj["code"] if "code" in sj else r.status_code
                error = sj["message"] if "message" in sj else r.content.decode("utf8")
                return (False, dict(code=code, data=sj, error=error))
        return (False, dict(code=500, error="Failed to generate csv results"))

    def results_export_logs(self, test_id, status_wait=60):
        r = self.post(self._url("/api/v2/results/%s/operations/export-results" % (test_id,)))
        self.logger.debug("results_export_logs: %r", r.content)
        if r.status_code not in (requests.codes.ok, requests.codes.accepted):
            self.logger.error("failed to gather logs for test %r", test_id)
            return (False, dict(code=r.status_code, error=r.content))
        j = r.json()
        op_id = j["id"]

        while status_wait > 0:
            time.sleep(4)
            status_wait -= 4
            r = self.get(self._url("/api/v2/results/%s/operations/generate-results/%s" \
                                   % (test_id, str(op_id))))
            if r.status_code != requests.codes.ok:
                self.logger.debug("results_export_logs op status: %r", r.status_code)
                continue
            sj = r.json()
            if sj["state"] == "SUCCESS":
                captures = dict()
                config_data = {}
                config_metadata = {}
                r = self.get(self._url(sj["resultUrl"]))
                ff = io.BytesIO(r.content)
                z = zipfile.ZipFile(ff)
                for zn in z.namelist():
                    if not zn.endswith(".zip"):
                        continue
                    if not zn.startswith("agent-") and not zn.startswith("test_config"):
                        continue
                    with z.open(zn) as zf:
                        zc = zf.read()
                        z2 = zipfile.ZipFile(io.BytesIO(zc))
                        if zn.startswith("agent-"):
                            for zn2 in z2.namelist():
                                if not zn2.endswith(".pcap"):
                                    continue
                                with z2.open(zn2) as zf2:
                                    captures[os.path.basename(zn2)] = base64.b64encode(zf2.read()).decode("utf8")
                        elif zn.startswith("test_config"):
                            with z2.open("config-data.bin") as cd:
                                config_data = json.loads(cd.read().decode("utf8"))
                            with z2.open("config-metadata.json") as cm:
                                config_metadata = json.loads(cm.read().decode("utf8"))
                return (True, dict(code=r.status_code, captures=captures, config_data=config_data,
                                   config_metadata=config_metadata)) # data=base64.b64encode(r.content),
            if sj["state"] == "ERROR":
                self.logger.error("failed to collect generated captures and logs")
                code = sj["code"] if "code" in sj else r.status_code
                error = sj["message"] if "message" in sj else r.content.decode("utf8")
                return (False, dict(code=code, data=sj, error=error))
        return (False, dict(code=500, error="Failed to export captures and logs"))
