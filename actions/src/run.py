from __future__ import print_function

import os
import sys
import time
import json
import requests

from st2common.runners.base_action import Action
from api import KeysightRICtestAPI


class KeysightRICtestAction(Action):

    APIMAP = {
        "config.find": "config_find",
        "config.create": "config_create",
        "config.delete": "config_delete",
        "session.find": "session_find",
        "session.create": "session_create",
        "session.delete": "session_delete",
        "session.test.start": "session_test_start",
        "session.test.status": "session_test_status",
        "session.test.stop": "session_test_stop",
        "results.generate_csv": "results_generate_csv",
        "results.export_logs": "results_export_logs",
        "agent.list": "agent_list",
        "agent.find": "agent_find",
        "agent.find_first": "agent_find_first",
    }

    def __init__(self, config):
        super(KeysightRICtestAction, self).__init__(config)

    def api(self, **kwargs):
        return KeysightRICtestAPI(
            endpoint=kwargs.pop("rictest_endpoint"),
            username=kwargs.pop("rictest_username"),
            password=kwargs.pop("rictest_password"),
            logger=self.logger)

    def agent_list(self, **kwargs):
        self.logger.debug("agent_list")
        return self.api(**kwargs).agent_list()

    def agent_find(self, **kwargs):
        args = (kwargs.pop("agent_id"),)
        self.logger.debug("agent_find%r", args)
        return self.api(**kwargs).agent_find(*args)

    def agent_find_first(self, **kwargs):
        args = (kwargs.pop("agent_id"), kwargs.pop("agent_interface"))
        self.logger.debug("agent_find_first%r", args)
        return self.api(**kwargs).agent_find_first(*args)

    def config_find(self, **kwargs):
        args = (kwargs.pop("config_id"),)
        self.logger.debug("config_find%r", args)
        return self.api(**kwargs).config_find(*args)

    def config_create(self, **kwargs):
        args = (kwargs.pop("config_template"), kwargs.pop("config_name"),
                kwargs.pop("ric_e2_addr"), kwargs.pop("ric_e2_port"),
                kwargs.pop("ric_gw_addr"), kwargs.pop("ric_gw_prefix"),
                kwargs.pop("oenb_count"), kwargs.pop("ognb_count"),
                kwargs.pop("ue_count"), kwargs.pop("sustain"), kwargs.pop("delay"),
                kwargs.pop("ramp_up_rate"), kwargs.pop("ramp_down_rate"),
                kwargs.pop("agent_id"), kwargs.pop("agent_interface"))
        self.logger.debug("config_create%r", args)
        return self.api(**kwargs).config_create(*args)

    def config_delete(self, **kwargs):
        args = (kwargs.pop("config_id"),)
        self.logger.debug("config_delete%r", args)
        return self.api(**kwargs).config_delete(*args)

    def session_create(self, **kwargs):
        args = (kwargs.pop("config_id"),)
        self.logger.debug("session_create%r", args)
        return self.api(**kwargs).session_create(*args)

    def session_find(self, **kwargs):
        args = (kwargs.pop("session_id"),)
        self.logger.debug("session_find%r", args)
        return self.api(**kwargs).session_find(*args)

    def session_delete(self, **kwargs):
        args = (kwargs.pop("session_id"),)
        self.logger.debug("session_delete%r", args)
        return self.api(**kwargs).session_delete(*args)

    def session_test_start(self, **kwargs):
        args = (kwargs.pop("session_id"),)
        self.logger.debug("session_test_start%r", args)
        return self.api(**kwargs).session_test_start(*args)

    def session_test_status(self, **kwargs):
        args = (kwargs.pop("session_id"), kwargs.pop("test_id"))
        self.logger.debug("session_test_status%r", args)
        return self.api(**kwargs).session_test_status(*args)

    def session_test_stop(self, **kwargs):
        args = (kwargs.pop("session_id"),)
        self.logger.debug("session_test_stop%r", args)
        return self.api(**kwargs).session_test_stop(*args)

    def results_generate_csv(self, **kwargs):
        args = (kwargs.pop("test_id"),)
        self.logger.debug("results_generate_csv%r", args)
        return self.api(**kwargs).results_generate_csv(*args)

    def results_export_logs(self, **kwargs):
        args = (kwargs.pop("test_id"),)
        self.logger.debug("results_export_logs%r", args)
        return self.api(**kwargs).results_export_logs(*args)

    def run(self, **kwargs):
        action = kwargs.pop("action")
        if not action in self.APIMAP:
            print("Unknown action %r", action, file=sys.stderr)
            exit(1)

        (ret, result) = getattr(self,self.APIMAP[action])(**kwargs)
        if not ret:
            if result:
                if isinstance(result, dict):
                    print(json.dumps(result))
                    exit(result["code"] if "code" in result else 1)
                else:
                    print(result)
                    exit(1)
            else:
                exit(1)
        return result
