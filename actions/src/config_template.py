import json

default_config_template = json.loads("""
{
    "displayName": "onf-1",
    "tags": {
        "category": "RIC"
    },
    "type": "wireless",
    "readonly": false,
    "configData": {
        "Config": null,
        "ConfigType": "RIC",
        "IRATConfig": null,
        "LicenseType": "",
        "Name": "onf-1",
        "SBAConfig": null,
        "CoreSimConfig": null,
        "SMOConfig": null,
        "RICESConfig": null,
        "LinkedResources": null,
        "UpfIsolationConfig": null,
        "owner": "admin@example.org",
        "RICConfig": {
            "version": "2.2.4-3575-166",
            "appsecCfgUrl": "",
            "globalSettings": {
                "advancedStats": {
                    "automatedPollingInterval": true,
                    "controlplane": false,
                    "customPollingInterval": 3,
                    "enableLoCapture": false,
                    "enablePerSessionStats": false,
                    "userplane": "none"
                },
                "cells": [
                    {
                        "cellId": 1,
                        "cellType": "CTNR",
                        "id": "1",
                        "plmn": {
                            "mcc": "226",
                            "mnc": "09"
                        },
                        "restrictedSliceIds": []
                    },
                    {
                        "cellId": 2,
                        "cellType": "CTEUTRA",
                        "id": "2",
                        "plmn": {
                            "mcc": "226",
                            "mnc": "09"
                        },
                        "restrictedSliceIds": null
                    }
                ],
                "flows": [
                    {
                        "fiveQi": 6,
                        "id": "1",
                        "isDefault": true,
                        "packetFilterList": [],
                        "preEmptionCapability": false,
                        "preEmptionVulnerability": false,
                        "priorityLevelARP": 0,
                        "qfi": 5,
                        "qosRulePrecedence": 0,
                        "useMatchAllPacketFilter": false
                    },
                    {
                        "appId": "",
                        "averagingWindow": 2000,
                        "dualConnectivityFlow": false,
                        "fiveQi": 1,
                        "gbr": {
                            "downlinkBitrateUnit": "u1Kbps",
                            "downlinkBitrateValue": 1,
                            "uplinkBitrateUnit": "u1Kbps",
                            "uplinkBitrateValue": 1
                        },
                        "id": "2",
                        "isDefault": false,
                        "isPredefinedOnSMF": false,
                        "mappedEBI": 5,
                        "maxDataBurstVolume": 2000,
                        "maxPacketLossRate": {
                            "downlink": 0,
                            "uplink": 0
                        },
                        "mbr": {
                            "downlinkBitrateUnit": "u1Kbps",
                            "downlinkBitrateValue": 1,
                            "uplinkBitrateUnit": "u1Kbps",
                            "uplinkBitrateValue": 1
                        },
                        "networkRequested": {
                            "delay": 0,
                            "enable": false,
                            "interval": 5,
                            "loops": 1
                        },
                        "notificationControl": false,
                        "overrideBitratesWithValuesDerivedFromAAR": true,
                        "packetDelayBudget": 300,
                        "packetErrorRate": "1E-6",
                        "packetFilterList": [
                            {
                                "direction": "bidirectional",
                                "id": "1",
                                "ipv4RemoteAddress": "192.168.0.22/255.255.0.0",
                                "ipv6RemoteAddressAndPrefixLength": "2001::1:11d2:8f92:64bc:dff0/64",
                                "protocolIdentifierOrNextHeader": 1,
                                "securityParameterIndex": 1,
                                "singleLocalPort": 1324,
                                "singleRemotePort": 1234
                            }
                        ],
                        "pccRuleName": "",
                        "preEmptionCapability": false,
                        "preEmptionVulnerability": false,
                        "priorityLevel": 60,
                        "priorityLevelARP": 15,
                        "qfi": 5,
                        "qosRulePrecedence": 255,
                        "resourceType": "NON_GBR",
                        "segregation": false,
                        "type": "data",
                        "useMatchAllPacketFilter": true,
                        "usePrecedenceForAppId": true
                    }
                ],
                "impairments": [],
                "logLevel": "info",
                "slices": [
                    {
                        "capacity": {
                            "enable": false,
                            "reservedCapacityDL": 50,
                            "reservedCapacityUL": 50
                        },
                        "gbr": {
                            "downlink": 19999,
                            "downlinkUnit": "bps",
                            "enable": false,
                            "uplink": 1999,
                            "uplinkUnit": "bps"
                        },
                        "id": "1",
                        "nonGbr": {
                            "downlink": 19999,
                            "downlinkUnit": "Mbps",
                            "enable": true,
                            "uplink": 1999,
                            "uplinkUnit": "Mbps"
                        },
                        "qosFlowsIds": [
                            "1"
                        ],
                        "snssai": {
                            "id": "1",
                            "sd": 1000,
                            "sst": 1
                        }
                    },
                    {
                        "capacity": {
                            "enable": false,
                            "reservedCapacityDL": 50,
                            "reservedCapacityUL": 50
                        },
                        "gbr": {
                            "downlink": 2000,
                            "downlinkUnit": "Mbps",
                            "enable": true,
                            "uplink": 2000,
                            "uplinkUnit": "Mbps"
                        },
                        "id": "2",
                        "isGlobal": false,
                        "nonGbr": {
                            "downlink": 1000,
                            "downlinkUnit": "bps",
                            "enable": false,
                            "uplink": 2000,
                            "uplinkUnit": "bps"
                        },
                        "qosFlowsIds": [
                            "2"
                        ],
                        "snssai": {
                            "id": "",
                            "mappedSD": 100,
                            "mappedSST": 10,
                            "sd": 1000,
                            "sst": 2
                        }
                    }
                ]
            },
            "name": "onf-1",
            "networkSettings": {
                "agentsInfos": [
                    {
                        "agentId": "0079d626-30dc-48b7-a882-e055f3fac439",
                        "id": "",
                        "impairmentId": "-1",
                        "interfacesSettings": [
                            {
                                "capture": true,
                                "interfaceMac": "52:54:00:5d:32:99",
                                "interfaceName": "enp6s0",
                                "networkStack": "linuxStack",
                                "sriov": false
                            }
                        ]
                    }
                ]
            },
            "nodes": {
                "oducu": {
                    "settings": {
                        "enable": false,
                        "mappedAgents": []
                    },
                    "simulated": {
                        "ocuNodes": [
                            {
                                "cellIds": [
                                    "1"
                                ],
                                "count": 1,
                                "enable": true,
                                "id": "20000",
                                "interfaces": {
                                    "e2": {
                                        "connectivitySettings": {
                                            "firstVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            },
                                            "gwIncrement": "0.0.0.0",
                                            "gwStart": "10.254.254.1",
                                            "ipPrefix": 24,
                                            "localIPAddress": "30.1.1.1",
                                            "macIncrement": "000000000001",
                                            "macStart": "0031BB000001",
                                            "mss": 1460,
                                            "mtu": 1500,
                                            "secondVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            }
                                        }
                                    }
                                },
                                "isDut": false,
                                "nodeSettings": {
                                    "name": "O-CU-1"
                                },
                                "serviceModel": {
                                    "kpm": {
                                        "enable": true
                                    },
                                    "customac": {
                                        "enable": false
                                    }
                                },
                                "sliceIds": [
                                    "1"
                                ]
                            }
                        ],
                        "oduNodes": [
                            {
                                "cellIds": [
                                    "1"
                                ],
                                "count": 1,
                                "enable": true,
                                "id": "30000",
                                "interfaces": {
                                    "e2": {
                                        "connectivitySettings": {
                                            "firstVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            },
                                            "gwIncrement": "0.0.0.0",
                                            "gwStart": "10.254.254.1",
                                            "ipPrefix": 24,
                                            "localIPAddress": "40.1.1.1",
                                            "macIncrement": "000000000001",
                                            "macStart": "0041BB000001",
                                            "mss": 1460,
                                            "mtu": 1500,
                                            "secondVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            }
                                        }
                                    }
                                },
                                "isDut": false,
                                "nodeSettings": {
                                    "name": "O-DUCU-DU-1"
                                },
                                "serviceModel": {
                                    "kpm": {
                                        "enable": true
                                    },
                                    "customac": {
                                        "enable": false
                                    }
                                },
                                "sliceIds": [
                                    "1"
                                ]
                            }
                        ]
                    }
                },
                "odusplitcu": {
                    "settings": {
                        "enable": false,
                        "mappedAgents": []
                    },
                    "simulated": {
                        "ocucpNodes": [
                            {
                                "cellIds": [
                                    "1"
                                ],
                                "count": 1,
                                "enable": true,
                                "id": "40000",
                                "interfaces": {
                                    "e2": {
                                        "connectivitySettings": {
                                            "firstVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            },
                                            "gwIncrement": "0.0.0.0",
                                            "gwStart": "10.254.254.1",
                                            "ipPrefix": 24,
                                            "localIPAddress": "50.1.1.1",
                                            "macIncrement": "000000000001",
                                            "macStart": "0051BB000001",
                                            "mss": 1460,
                                            "mtu": 1500,
                                            "secondVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            }
                                        }
                                    }
                                },
                                "isDut": false,
                                "nodeSettings": {
                                    "name": "O-CUCP-1"
                                },
                                "serviceModel": {
                                    "kpm": {
                                        "enable": true
                                    },
                                    "customac": {
                                        "enable": false
                                    }
                                },
                                "sliceIds": [
                                    "1"
                                ]
                            }
                        ],
                        "ocuupNodes": [
                            {
                                "cellIds": [
                                    "1"
                                ],
                                "count": 1,
                                "enable": true,
                                "id": "50000",
                                "interfaces": {
                                    "e2": {
                                        "connectivitySettings": {
                                            "firstVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            },
                                            "gwIncrement": "0.0.0.0",
                                            "gwStart": "10.254.254.1",
                                            "ipPrefix": 24,
                                            "localIPAddress": "60.1.1.1",
                                            "macIncrement": "000000000001",
                                            "macStart": "0061BB000001",
                                            "mss": 1460,
                                            "mtu": 1500,
                                            "secondVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            }
                                        }
                                    }
                                },
                                "isDut": false,
                                "nodeSettings": {
                                    "name": "O-CUUP-1"
                                },
                                "serviceModel": {
                                    "kpm": {
                                        "enable": true
                                    },
                                    "customac": {
                                        "enable": false
                                    }
                                },
                                "sliceIds": [
                                    "1"
                                ]
                            }
                        ],
                        "oduNodes": [
                            {
                                "cellIds": [
                                    "1"
                                ],
                                "count": 1,
                                "enable": true,
                                "id": "60000",
                                "interfaces": {
                                    "e2": {
                                        "connectivitySettings": {
                                            "firstVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            },
                                            "gwIncrement": "0.0.0.0",
                                            "gwStart": "10.254.254.1",
                                            "ipPrefix": 24,
                                            "localIPAddress": "70.1.1.1",
                                            "macIncrement": "000000000001",
                                            "macStart": "0071BB000001",
                                            "mss": 1460,
                                            "mtu": 1500,
                                            "secondVLAN": {
                                                "enable": false,
                                                "idStart": 0,
                                                "tpid": "h8100"
                                            }
                                        }
                                    }
                                },
                                "isDut": false,
                                "nodeSettings": {
                                    "name": "O-DUSplitCU-DU-1"
                                },
                                "serviceModel": {
                                    "kpm": {
                                        "enable": true
                                    },
                                    "customac": {
                                        "enable": false
                                    }
                                },
                                "sliceIds": [
                                    "1"
                                ]
                            }
                        ]
                    }
                },
                "oenb": {
                    "ranges": [
                        {
                            "cellIds": [
                                "2"
                            ],
                            "count": 1,
                            "enable": true,
                            "id": "1",
                            "interfaces": {
                                "e2": {
                                    "connectivitySettings": {
                                        "firstVLAN": {
                                            "enable": false,
                                            "idStart": 0,
                                            "tpid": "h8100"
                                        },
                                        "gwIncrement": "0.0.0.0",
                                        "gwStart": "10.254.254.1",
                                        "ipPrefix": 24,
                                        "localIPAddress": "10.254.254.120",
                                        "macIncrement": "000000000001",
                                        "macStart": "0011BB000001",
                                        "mss": 1460,
                                        "mtu": 1500,
                                        "secondVLAN": {
                                            "enable": false,
                                            "idStart": 0,
                                            "tpid": "h8100"
                                        }
                                    }
                                }
                            },
                            "isDut": false,
                            "nodeSettings": {
                                "name": "O-eNB-1"
                            },
                            "serviceModel": {
                                "kpm": {
                                    "enable": true
                                },
                                "customac": {
                                    "enable": false
                                }
                            },
                            "sliceIds": [
                                "1",
                                "2"
                            ]
                        }
                    ],
                    "settings": {
                        "enable": true,
                        "mappedAgents": [
                            {
                                "agentId": "0079d626-30dc-48b7-a882-e055f3fac439",
                                "id": "",
                                "interfaceMappings": [
                                    {
                                        "agentInterface": "enp6s0",
                                        "agentInterfaceMac": "52:54:00:5d:32:99",
                                        "nodeInterface": "e2"
                                    }
                                ]
                            }
                        ]
                    }
                },
                "ognb": {
                    "ranges": [
                        {
                            "cellIds": [
                                "1"
                            ],
                            "count": 1,
                            "enable": true,
                            "id": "10000",
                            "interfaces": {
                                "e2": {
                                    "connectivitySettings": {
                                        "firstVLAN": {
                                            "enable": false,
                                            "idStart": 0,
                                            "tpid": "h8100"
                                        },
                                        "gwIncrement": "0.0.0.0",
                                        "gwStart": "10.254.254.1",
                                        "ipPrefix": 24,
                                        "localIPAddress": "10.254.254.20",
                                        "macIncrement": "000000000001",
                                        "macStart": "0021BB000001",
                                        "mss": 1460,
                                        "mtu": 1500,
                                        "secondVLAN": {
                                            "enable": false,
                                            "idStart": 0,
                                            "tpid": "h8100"
                                        }
                                    }
                                }
                            },
                            "isDut": false,
                            "nodeSettings": {
                                "name": "O-gNB-1"
                            },
                            "serviceModel": {
                                "kpm": {
                                    "enable": true
                                },
                                "customac": {
                                    "enable": false
                                }
                            },
                            "sliceIds": [
                                "1",
                                "2"
                            ]
                        }
                    ],
                    "settings": {
                        "enable": true,
                        "mappedAgents": [
                            {
                                "agentId": "0079d626-30dc-48b7-a882-e055f3fac439",
                                "id": "",
                                "interfaceMappings": [
                                    {
                                        "agentInterface": "enp6s0",
                                        "agentInterfaceMac": "52:54:00:5d:32:99",
                                        "nodeInterface": "e2"
                                    }
                                ]
                            }
                        ]
                    }
                },
                "ric": {
                    "ranges": [
                        {
                            "count": 1,
                            "enable": true,
                            "id": "1",
                            "interfaces": {
                                "e2": {
                                    "connectivitySettings": {
                                        "firstVLAN": {
                                            "enable": false,
                                            "idStart": 0,
                                            "tpid": "h8100"
                                        },
                                        "gwIncrement": "0.0.0.0",
                                        "gwStart": "0.0.0.0",
                                        "ipPrefix": 24,
                                        "localIPAddress": "10.101.32.36",
                                        "macIncrement": "000000000001",
                                        "macStart": "0111BB000001",
                                        "mss": 1460,
                                        "mtu": 1500,
                                        "port": 36421,
                                        "secondVLAN": {
                                            "enable": false,
                                            "idStart": 0,
                                            "tpid": "h8100"
                                        }
                                    }
                                }
                            },
                            "serviceModel": {
                                "kpm": {
                                    "enable": true
                                },
                                "customac": {
                                    "enable": false
                                }
                            },
                            "isDut": true
                        }
                    ],
                    "settings": {
                        "enable": true,
                        "mappedAgents": []
                    }
                },
                "ue": {
                    "ranges": [
                        {
                            "count": 100,
                            "enable": true,
                            "id": "1",
                            "identification": {
                                "plmn": {
                                    "mcc": "226",
                                    "mnc": "09"
                                },
                                "startId": {
                                    "type": "string",
                                    "value": "0000001"
                                }
                            },
                            "parentNodeId": "10000",
                            "settings": {
                                "nssai": [
                                    "1",
                                    "2"
                                ]
                            },
                            "timelines": [
                                {
                                    "cellRangeId": "1",
                                    "delay": 0,
                                    "id": "1",
                                    "rampDownRate": 50,
                                    "rampUpRate": 50,
                                    "sustain": 120,
                                    "trafficItemId": "1"
                                }
                            ]
                        },
                        {
                            "count": 100,
                            "enable": true,
                            "id": "2",
                            "identification": {
                                "plmn": {
                                    "mcc": "226",
                                    "mnc": "09"
                                },
                                "startId": {
                                    "type": "string",
                                    "value": "0100001"
                                }
                            },
                            "parentNodeId": "1",
                            "settings": {
                                "nssai": [
                                    "1",
                                    "2"
                                ]
                            },
                            "timelines": [
                                {
                                    "cellRangeId": "2",
                                    "delay": 15,
                                    "id": "2",
                                    "rampDownRate": 50,
                                    "rampUpRate": 50,
                                    "sustain": 120,
                                    "trafficItemId": "2"
                                }
                            ]
                        }
                    ],
                    "trafficItems": [
                        {
                            "id": "1",
                            "qosIndicator": "1",
                            "throughputDownlink": {
                                "channelQualityGradient": "STEADY",
                                "maxSINR": 22.7,
                                "minSINR": -6.7,
                                "rate": 2,
                                "steadySINR": 22.7,
                                "throughputGradient": "STEADY",
                                "unit": "Kbps"
                            },
                            "throughputUplink": {
                                "channelQualityGradient": "STEADY",
                                "maxSINR": 22.7,
                                "minSINR": -6.7,
                                "rate": 2,
                                "steadySINR": 22.7,
                                "throughputGradient": "STEADY",
                                "unit": "Kbps"
                            },
                            "trafficType": "KPM"
                        },
                        {
                            "id": "2",
                            "qosIndicator": "2",
                            "throughputDownlink": {
                                "channelQualityGradient": "STEADY",
                                "maxSINR": 22.7,
                                "minSINR": -6.7,
                                "rate": 2000,
                                "steadySINR": 22.7,
                                "throughputGradient": "STEADY",
                                "unit": "bps"
                            },
                            "throughputUplink": {
                                "channelQualityGradient": "STEADY",
                                "maxSINR": 22.7,
                                "minSINR": -6.7,
                                "rate": 2000,
                                "steadySINR": 22.7,
                                "throughputGradient": "STEADY",
                                "unit": "bps"
                            },
                            "trafficType": "KPM"
                        }
                    ]
                }
            }
        }
    },
    "application": "",
    "encodedFiles": false,
    "linkedResources": null
}
""")
